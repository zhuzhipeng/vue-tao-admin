/*
 * @Description:
 * @Author: zzp
 * @Date: 2022-03-03 10:51:57
 * @LastEditTime: 2022-03-04 18:07:18
 * @LastEditors: zzp
 * @Reference:
 */
import axios from '@/utils/request'

export function getAdminList(params) {
  return axios.get({
    url: '/admin/admin/list',
    data: params
  }).then((res) => {
    return res;
  })
}
export function editAdmin(params) {
  return axios.post({
    url: "/admin/admin/edit",
    data: params
  }).then((res) => {
    return res
  })
}
export function deleteAdmin(params) {
  return axios.post({
    url: "/admin/admin/delete",
    data: params
  }).then((res) => {
    return res
  })
}
export function modifyAdmin(params) {
  return axios.post({
    url: "/admin/admin/modify",
    data: params
  }).then((res) => {
    return res
  })
}
