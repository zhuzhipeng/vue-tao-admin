/*
 * @Description:
 * @Author: zzp
 * @Date: 2022-03-03 10:51:57
 * @LastEditTime: 2022-03-04 18:07:18
 * @LastEditors: zzp
 * @Reference:
 */
import axios from '@/utils/request'

export function getAgentList(params) {
  return axios.get({
    url: '/admin/agent/list',
    data: params
  }).then((res) => {
    return res;
  })
}
export function editAgent(params) {
  return axios.post({
    url: "/admin/admin/edit",
    data: params
  }).then((res) => {
    return res
  })
}
export function deleteAgent(params) {
  return axios.post({
    url: "/admin/admin/delete",
    data: params
  }).then((res) => {
    return res
  })
}
export function modifyAgent(params) {
  return axios.post({
    url: "/admin/admin/modify",
    data: params
  }).then((res) => {
    return res
  })
}
export function getAllAgent(params){
  return axios.get({
    url: '/admin/agent/all',
    data: params
  }).then((res) => {
    return res;
  })
}
