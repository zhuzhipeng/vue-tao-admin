/*
 * @Description:
 * @Author: zzp
 * @Date: 2022-02-11 16:49:55
 * @LastEditTime: 2022-03-04 09:27:56
 * @LastEditors: zzp
 * @Reference:
 */
import axios from '@/utils/request'
import store from '../store'
import router from '../router'
import { allRoutes } from '@/router/index.js'
import { routerMatch } from '@/utils/menu.js'
import { menuData } from '@/mock/menuData.js'

// 模拟获取菜单数据
export function getMenuList() {
  let sys = JSON.parse(localStorage.getItem("sys"))
  let isLogin = false

  if (sys) {
    isLogin = sys.user.isLogin
  }

  if (!isLogin) {
    return
  }
  axios.get({
    url: '/admin/menu/getMenu'
  }).then((res) => {
    //console.log('@@', res);
    var menuData = res.data;
    routerMatch(menuData, allRoutes).then(routes => {
      store.dispatch('menu/setMenuList', menuData)
      router.options.routes = Array.from(
        new Set(router.options.routes.concat(routes))
      )
      router.addRoutes(routes) // 动态添加路由
    })
  })
  // routerMatch(menuData, allRoutes).then(routes => {
  //   store.dispatch('menu/setMenuList', menuData)
  //   router.options.routes = Array.from(
  //     new Set(router.options.routes.concat(routes))
  //   )
  //   router.addRoutes(routes) // 动态添加路由
  // })
}
//获取所有顶级菜单
export function getTopMenu(params) {
  return axios.post({
    url: '/api/getApiList',
    data: params
  }).then((res) => {
    return res
  })
}
//获取菜单tree
export function getAllMenuTree(params) {
  return axios.get({
    url: '/admin/menu/getAllMenu',
    data: params
  }).then((res) => {
    //console.log('bbbbbbb',res);
    // var menuData = res.data;
    // store.dispatch('menu/setAllMenuList', menuData)
    //return res.data
    return res;
  })
}
export function editMenu(params) {
  return axios.post({
    url: '/admin/menu/edit',
    data: params
  }).then((res) => {
    return res
  })
}

