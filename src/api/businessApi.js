
import axios from '@/utils/request'

export function getBusinessCateList(params) {
  return axios.get({
    url: '/admin/businesscate/lst',
    data: params
  }).then((res) => {
    return res;
  })
}
export function getBusinessAllCate(params) {
  return axios.get({
    url: '/admin/businesscate/all',
    data: params
  }).then((res) => {
    return res;
  })
}
export function editCate(params) {
  return axios.post({
    url: "/admin/businesscate/edit",
    data: params
  }).then((res) => {
    return res;
  })
}
export function deleteCate(params) {
  return axios.get({
    url: "/admin/businesscate/del",
    data: params
  }).then((res) => {
    return res;
  })
}
export function getBuinessList(params) {
  return axios.get({
    url: "/admin/business/lst",
    data: params
  }).then((res) => {
    return res;
  })
}
