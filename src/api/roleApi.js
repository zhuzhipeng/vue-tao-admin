/*
 * @Description:
 * @Author: zzp
 * @Date: 2022-03-03 10:51:57
 * @LastEditTime: 2022-03-04 16:45:52
 * @LastEditors: zzp
 * @Reference:
 */
import axios from '@/utils/request'

export function getRoleList(params) {
  return axios.get({
    url: '/admin/role/list',
    data: params
  }).then((res) => {
    return res;
  })
}
export function getRoleInfo(params) {
  return axios.get({
    url: '/admin/role/info',
    data: params
  }).then((res) => {
    return res;
  })
}
export function editRole(params) {
  return axios.post({
    url: "/admin/role/edit",
    data: params
  }).then((res) => {
    return res
  })
}
export function deleteRole(params) {
  return axios.post({
    url: "/admin/role/delete",
    data: params
  }).then((res) => {
    return res
  })
}
export function modifyRole(params) {
  return axios.post({
    url: "/admin/role/modify",
    data: params
  }).then((res) => {
    return res
  })
}
