/*
 * @Description:
 * @Author: zzp
 * @Date: 2022-03-03 10:51:57
 * @LastEditTime: 2022-03-04 18:07:18
 * @LastEditors: zzp
 * @Reference:
 */
import axios from '@/utils/request'

export function getAddressTree(params) {
  return axios.get({
    url: '/admin/address/getTree',
    data: params
  }).then((res) => {
    return res;
  })
}

