import axios from '@/utils/request';

export function loginApi(params) {
  return axios.post({
    url: '/admin/login',
    data: params
  }).then((res) => {
    return res
  })
}

export function getCaptchaApi(params) {
  return axios.get({
    url: '/admin/captcha',
    data: params
  }).then((res) => {
    return res
  })
}
