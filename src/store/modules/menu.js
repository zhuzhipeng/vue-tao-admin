/*
 * @Description:
 * @Author: zzp
 * @Date: 2022-02-11 16:49:55
 * @LastEditTime: 2022-03-02 11:05:33
 * @LastEditors: zzp
 * @Reference:
 */
/**
 * 菜单|权限列表
 */

const state = {
  menuList: [],
  menuWidth: '',
  menuOpen: true,
  allMenuList:[],
}

const getters = {
  menu: (state) => state.menuList
}

const mutations = {
  setMenuList(state, list) {
    state.menuList = list
  },
  setAllMenuList(state, list) {
    state.allMenuList = list
  },
  setMenuWidth(state, e) {
    state.menuWidth = e
  },
  setMenuOpen(state, e) {
    state.menuOpen = e
  }
}

const actions = {
  setMenuList({commit}, e) {
    commit('setMenuList', e)
  },
  setAllMenuList({commit}, e) {
    commit('setAllMenuList', e)
  },
  setMenuWidth({commit}, e) {
    commit('setMenuWidth', e)
  },
  setMenuOpen({commit}, e) {
    commit('setMenuOpen', e)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
